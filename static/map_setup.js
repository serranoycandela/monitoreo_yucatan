var layer_url = document.currentScript.getAttribute('layer_url');
var data_url = document.currentScript.getAttribute('data_url');
var year = document.currentScript.getAttribute('year');
var uga = document.currentScript.getAttribute('uga');
document.getElementById('map_title').innerHTML = "<h1><center>2022-"+year+"</center></h1>";
next_year = parseInt(year)+1;
prev_year = parseInt(year)-1;
document.getElementById("yforward").onclick = function () {
	location.href = "/monitoreo/"+next_year+"/"+uga;
};
document.getElementById("ybackward").onclick = function () {
	location.href = "/monitoreo/"+prev_year+"/"+uga;
};
const reducer = (accumulator, curr) => accumulator + curr;

function get_features(url) {
    var data_layer = {};

    $.ajax({
	url: url,
	async: false,
	dataType: 'json',
	success: function(data) {
	    data_layer = data;
	}
    });
    var format_data_layer = new ol.format.GeoJSON();
    var features = format_data_layer.readFeatures(data_layer,
						  {dataProjection: 'EPSG:4326',
						   featureProjection: 'EPSG:3857'});

    return features;
}
//linear color scale
var colorscale = d3.scale.linear()
.domain([0,1])
.range(["plum", "purple"])
.interpolate(d3.interpolateLab);

function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);
    return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
}
var polygon_style2 = new ol.style.Style({
	  fill: new ol.style.Fill({color: hexToRGB(colorscale(0.9),0.65)}),
	  stroke: new ol.style.Stroke({color: hexToRGB(colorscale(0.9),1),width: 1}),
	  text: new ol.style.Text({
		  	font: '12px Calibri,sans-serif',
		  	fill: new ol.style.Fill({color: 'rgba(250,163,1,1)'}),
	        stroke: new ol.style.Stroke({
	        		color: 'rgba(100,100,100,1)',
	        		width: 3
	        })
	  })
});

var map
var vectorSource = new ol.source.Vector({projection: 'EPSG:4326'});
var miVector = new ol.layer.Vector({
    	source: vectorSource
});
var layer = new ol.layer.Vector();
jsonSource_data_layer = new ol.source.Vector();
jsonSource_data_layer.addFeatures(get_features(layer_url));
vectorSource.addFeatures(get_features(layer_url));
var todos = jsonSource_data_layer.getFeatures();
var geometry_type = todos[0].getGeometry().getType();
layer = new ol.layer.Vector({
    source: jsonSource_data_layer,
	opacity: 0.65
});
if ((geometry_type == "Polygon") || (geometry_type == "MultiPolygon")){
	layer.setStyle(polygon_style);
	miVector.setStyle(polygon_style2);
}

if (geometry_type == "Point" || geometry_type == "MultiPoint"){
	layer.setStyle(point_style);
	miVector.setStyle(point_style2);
}

var stamenLayer = new ol.layer.Tile({
	source: new ol.source.Stamen({layer: 'toner'})
});
var ids = [];
todos.forEach(function(feature){ids.push(feature.get("id"))});

var estosFeatures = todos;

map = new ol.Map({
    projection:"EPSG:4326",
    layers: [stamenLayer, layer, miVector],
    target: 'map'
});

var extent = layer.getSource().getExtent();
map.getView().fit(extent, map.getSize());



var highlightStyleCache = {};
var highlight;

var displayFeatureInfo = function (pixel) {

	var feature = map.forEachFeatureAtPixel(pixel, function (feature) {
		    return feature;
	});

	if (feature) {
		pcz.highlight(pcz.data().filter(function(d) {
		    return d.id === feature.get('id');
		    }));
		
		c = document.getElementById("tot_proy_text");
		ancho = c.clientWidth;
		alto = c.clientHeight;
		uno = pcz.data().filter(function(d) {
            return d.id === feature.get('id');
            });
		total_proyectos = uno[0]["Suma"]
		document.getElementById("charts_header").innerHTML = "<h1> <center>Sumatorias para 1 UGA seleccionada</center></h1>";
		
		name_list = [uno[0]["nombre"]];
		updateCharts(name_list);
		texto = "<h1>Selección </br> </h1>"+'<div class="item">'+ feature.get("nombre") +'</div>';
		lista = document.getElementById("selected");
		lista.innerHTML = texto;
		
	}else{
		vectorSource.clear();
	    vectorSource.addFeatures(estosFeatures);
	    pcz.unhighlight();
		

		name_list = [];
		estosFeatures.forEach(function(feature){
			name_list.push(feature.get("nombre"));
		});
		
		texto = "<h1>Selección </br> ("+name_list.length+")</h1>";
		name_list.forEach(function(name){
			texto = texto + '<div class="item">'+ name +'</div>'
		});
		lista = document.getElementById("selected");
		lista.innerHTML = texto;
		updateCharts(name_list)
		
	}

	if (feature !== highlight) {
		vectorSource.clear();
	    if (feature) {
			vectorSource.addFeature(feature);
		}
		highlight = feature;
	}

};
var hover = true;
if (uga == "All"){
	console.log("scmvsñdmv");
}else{
	hover = false;
}
map.on('pointermove', function(evt) {
    if (evt.dragging) {
      return;
    }
	if (hover){
		var pixel = map.getEventPixel(evt.originalEvent);
		displayFeatureInfo(pixel);
	}
    
  });
map.on('click', function(evt) {
	if (hover){
		hover = false;
		var feature = map.forEachFeatureAtPixel(evt.pixel, function (feature) {
		    return feature;
		});

		if (feature) {
			uga = feature.get('id');
		}
		
	}else{
		hover = true;
		uga = "All";
		estosFeatures = todos.filter(function (feature) {return ids.indexOf(feature.get('id')) >= 0;});
	}
	document.getElementById("yforward").onclick = function () {
		location.href = "/monitoreo/"+next_year+"/"+uga;
	};
	document.getElementById("ybackward").onclick = function () {
		location.href = "/monitoreo/"+prev_year+"/"+uga;
	};
	displayFeatureInfo(evt.pixel);
});
map.getViewport().addEventListener('mouseout', function(evt){
	if (hover){
		vectorSource.clear();
		vectorSource.addFeatures(estosFeatures);
		pcz.unhighlight();
	}
}, false);

var pcz;
var tot_proy;
var sup_total;
var sup_selva;
var sup_manglar;
var agua_consu;
var agua_res;

d3.csv("/uploads/"+year+"/tot_proy.csv", function(data) {
    tot_proy = data;
});
d3.csv("/uploads/"+year+"/sup_total.csv", function(data) {
    sup_total = data;
});
d3.csv("/uploads/"+year+"/sup_selva.csv", function(data) {
    sup_selva = data;
});
d3.csv("/uploads/"+year+"/sup_manglar.csv", function(data) {
    sup_manglar = data;
});
d3.csv("/uploads/"+year+"/agua_consu.csv", function(data) {
    agua_consu = data;
});
d3.csv("/uploads/"+year+"/agua_res.csv", function(data) {
    agua_res = data;
});
function filterItems(arr, sel) {
	return arr.filter(function(el) {
	  return sel.includes(el.nombre);
	})
  }

function sumSelected(arr, sel) {
	fil = filterItems(arr, sel);
	result = fil.reduce(function(r, e) {  
  
		r.agri += parseFloat(e.agri);
		r.ener += parseFloat(e.ener);
		r.hidr += parseFloat(e.hidr);
		r.indu += parseFloat(e.indu);
		r.manejo += parseFloat(e.manejo);
		r.mine += parseFloat(e.mine);
		r.porc += parseFloat(e.porc);
		r.turi += parseFloat(e.turi);
		r.urba += parseFloat(e.urba);
		r.vial += parseFloat(e.vial);
	  
	  return r;
	}, {"agri":0,"ener":0,"hidr":0,"indu":0,"manejo":0,"mine":0,"porc":0,"turi":0,"urba":0,"vial":0});
	return Object.values(result);
}

makeChart = function(suDiv){
	var ctx = document.getElementById(suDiv).getContext('2d'); 
	var el_chart = new Chart(ctx, {
	type: 'bar',
	data: {
	  labels: ["Agricultura","Energía","Hidraúlico","Industrial","Manejo","Minería","Pecuario","Turismo","Urbano","Vial"],
	  datasets: [
		{
		  data: [1,1,1,1,1,1,1,1,1,1]
		}
	  ]
	},
	options: {
	  plugins: {
		  legend: { display: false }
	  }
	}
	});
	return el_chart;
};

makeStackedChart = function(suDiv){
	var ctx = document.getElementById(suDiv).getContext('2d'); 
	var el_chart = new Chart(ctx, {
		type: "bar",
		data: {
		  //labels: ["Agricultura","Energía","Hidraúlico","Industrial","Manejo","Minería","Pecuario","Turismo","Urbano","Vial"],
		  labels: [""],
		  datasets: [
			{
			  label: "Agricultura",
			  data: [727],
			  backgroundColor: "#f68b73"
			},
			{
				label: "Energía",
				data: [727],
				backgroundColor: "#83baa7"
			  },
			  {
				label: "Hidraúlico",
				data: [727],
				backgroundColor: "#5f8a58"
			  },
			  {
				label: "Industrial",
				data: [727],
				backgroundColor: "#bee6d9"
			  },
			  {
				label: "Residuos",
				data: [727],
				backgroundColor: "#c59f4e"
			  },
			  {
				label: "Minería",
				data: [727],
				backgroundColor: "#fad27e"
			  },
			  {
				label: "Pecuario",
				data: [727],
				backgroundColor: "#bdce64"
			  },
			  {
				label: "Turismo",
				data: [727],
				backgroundColor: "#eafc89"
			  },
			  {
				label: "Urbano",
				data: [727],
				backgroundColor: "#5f5c3e"
			  },
			  {
				label: "Vial",
				data: [727],
				backgroundColor: "#969585"
			  }
		  ]
		},
		options: {
			indexAxis: 'y',
			plugins: {
			   legend: { display: false },
			   annotation: {
					annotations: {
						line1: {
							adjustSacaleRange: true,
							drawTime: "afterDatasetsDraw",
							type: 'line',
							scaleID: 'x',
							// yScaleID: 'y',
							// xMin: 100,
							// xMax: 100,
							// yMin: 0,
							// yMax: 1,
							value: 0,
							borderColor: "red",
							borderWidth: 0
						  }
					},
					
				}
			},
			responsive: true,
			scales: {
			  x: {
				stacked: true,
			  },
			  y: {
				stacked: true
			  }
			}
		  }
	  });
	return el_chart;
};

var tot_proy_chart = makeStackedChart('tot_proy_bar');
var sup_total_chart = makeStackedChart('sup_total_bar');
var sup_selva_chart = makeStackedChart('sup_selva_bar');
var sup_manglar_chart = makeStackedChart('sup_manglar_bar');
var agua_consu_chart = makeStackedChart('agua_consu_bar');
var agua_res_chart = makeStackedChart('agua_res_bar');

updateCharts = function(name_list){
	document.getElementById("charts_header").innerHTML = "<h1>  <center> Sumatorias para " + name_list.length + " UGA seleccionadas </center></h1> <center><img src='/static/leyenda.png' alt='HTML5 Icon' style='width:700px;height:60px;'> </center>";
	tot_proy_sum = sumSelected(tot_proy, name_list);

	document.getElementById("tot_proy_text").innerHTML = "<br><h1> " + tot_proy_sum.reduce(reducer) + " proyectos aprobados</h1>";
	sup_total_sum = sumSelected(sup_total, name_list);
	document.getElementById("sup_total_text").innerHTML = "<br><h1> " + Math.round(sup_total_sum.reduce(reducer)) + " hectareas</h1>";
	sup_selva_sum = sumSelected(sup_selva, name_list);
	document.getElementById("sup_selva_text").innerHTML = "<br><h1>" + Math.round(sup_selva_sum.reduce(reducer)) + " hectareas de selva afectadas </h1>";
	sup_manglar_sum = sumSelected(sup_manglar, name_list);
	document.getElementById("sup_manglar_text").innerHTML = "<br><h1>" + Math.round(sup_manglar_sum.reduce(reducer)) + " hectareas de manglar afectadas </h1>";
	agua_consu_sum = sumSelected(agua_consu, name_list);
	document.getElementById("agua_consu_text").innerHTML = "<br><h1>" + Math.round(agua_consu_sum.reduce(reducer)) + " m3/día de consumo de agua</h1>";
	agua_res_sum = sumSelected(agua_res, name_list);
	document.getElementById("agua_res_text").innerHTML = "<br><h1>" + Math.round(agua_res_sum.reduce(reducer)) + " m3/s de aguas residuales</h1>";
	
	tot_proy_sum.forEach(function (item, index) {
		tot_proy_chart.data.datasets[index].data = [item];
	  });
	tot_proy_chart.update();

	sup_total_sum.forEach(function (item, index) {
		sup_total_chart.data.datasets[index].data = [item];
	  });
	sup_total_chart.update();

	sup_selva_sum.forEach(function (item, index) {
		sup_selva_chart.data.datasets[index].data = [item];
	  });
	if (name_list.length == 1){
		fil = filterItems(sup_selva, name_list);
		valor = fil[0].limite
		sup_selva_chart.config.options.plugins.annotation.annotations.line1.value = valor;
		sup_selva_chart.config.options.plugins.annotation.annotations.line1.borderWidth = 2;
		fil = filterItems(sup_manglar, name_list);
		valor = fil[0].limite
		sup_manglar_chart.config.options.plugins.annotation.annotations.line1.value = valor;
		sup_manglar_chart.config.options.plugins.annotation.annotations.line1.borderWidth = 2;
	}else{
		sup_selva_chart.config.options.plugins.annotation.annotations.line1.value = 0;
		sup_selva_chart.config.options.plugins.annotation.annotations.line1.borderWidth = 0;
		sup_manglar_chart.config.options.plugins.annotation.annotations.line1.value = 0;
		sup_manglar_chart.config.options.plugins.annotation.annotations.line1.borderWidth = 0;
	}
	sup_selva_chart.update();

	sup_manglar_sum.forEach(function (item, index) {
		sup_manglar_chart.data.datasets[index].data = [item];
	  });
	sup_manglar_chart.update();

	agua_consu_sum.forEach(function (item, index) {
		agua_consu_chart.data.datasets[index].data = [item];
	  });
	agua_consu_chart.update();

	agua_res_sum.forEach(function (item, index) {
		agua_res_chart.data.datasets[index].data = [item];
	  });
	agua_res_chart.update();

	
}
	
//load csv file and create the chart
d3.csv(data_url, function(data) {

	pcz = d3.parcoords()("#graph")
	 .data(data)
	 .hideAxis(["id","nombre","Suma"])
	 .composite("darken")
	 .mode("queue")
	 .rate(80)
	 .color(hexToRGB(colorscale(0.9),0.65))
	 //.alphaOnBrushed(0.3)
	 .render()
	 .alpha(1)
	 .brushMode("1D-axes")  
	 .interactive()  
	pcz.svg.selectAll(".dimension")
	 .on("click", change_color)
	 .selectAll(".label")
	 .style("font-size", "26px");
	pcz.on("brush", function(d) {
		vectorSource.clear();
		pcz.shadows();
		ids = [];
		name_list = [];
		total_proyectos = 0;
		d.forEach(function(entry) {
			ids.push(entry["id"]);
			name_list.push(entry["nombre"]);
			total_proyectos += parseInt(entry["Suma"]);
		});
		
		updateCharts(name_list);
		
		estosFeatures = todos.filter(function (feature) {return ids.indexOf(feature.get('id')) >= 0;});
		vectorSource.addFeatures(estosFeatures);
		
		texto = "<h1>Selección </br>  ("+name_list.length+")</h1>";
		name_list.forEach(function(name){
			texto = texto + '<div class="item">'+ name +'</div>'
		});
		lista = document.getElementById("selected");
		lista.innerHTML = texto;
	});
});



//update color
function change_color(dimension) {
    if (pcz.dimensions()[dimension].type == "number"){



        	pcz.svg.selectAll(".dimension")
        	 .style("font-weight", "normal")
        	 .filter(function(d) { return d == dimension; })
        	 .style("font-weight", "bold")
        	pcz.color(pre_color(pcz.data(),dimension)).render()



        	var polygon_style_p = function(feature, resolution){
        	    var context = {
        			feature: feature,
        			variables: {}
        	    };
        	    var value = feature.get(dimension);
        	    var slice = _(pcz.data()).pluck(dimension).map(parseFloat);
        		var normalize = d3.scale.linear()
        		.domain([_.min(slice),_.max(slice)])
        		.range([0,1]);
        	    var size = 0;
        	    var style = [ new ol.style.Style({
        		    	stroke: new ol.style.Stroke({
        			    		color: colorscale(normalize(value)),
        						lineDash: null,
        						lineCap: 'butt',
        						lineJoin: 'miter',
        						width: 0}),
        				fill: new ol.style.Fill({color: hexToRGB(colorscale(normalize(value)),0.65)})
        	    //colorscale(normalize(value))
        		})];
        	    if ("" !== null) {
        		var labelText = String("");
        	    } else {
        		var labelText = ""
        	    }
        	    var key = value + "_" + labelText

        	    if (!styleCache[key]){
        		var text = new ol.style.Text({
        		      font: '14.3px \'Ubuntu\', sans-serif',
        		      text: labelText,
        		      textBaseline: "center",
        		      textAlign: "left",
        		      offsetX: 5,
        		      offsetY: 3,
        		      fill: new ol.style.Fill({
        		        color: 'rgba(0, 0, 0, 255)'
        		      }),
        		    });
        		styleCache[key] = new ol.style.Style({"text": text})
        	    }
        	    var allStyles = [styleCache[key]];
        	    allStyles.push.apply(allStyles, style);
        	    return allStyles;
        	};

        	miVector.setStyle(polygon_style_p);
     }

}

function pre_color(col,dimension){
	var slice = _(col).pluck(dimension).map(parseFloat);
	var normalize = d3.scale.linear()
	.domain([_.min(slice),_.max(slice)])
	.range([0,1]);
	return function(d) { return colorscale(normalize(d[dimension])) }
}

$(window).bind("load", function() {
	//change_color("vulnera",true);
	name_list = [];
	console.log("----------------------------------------------------------"+uga);
	if (uga == "All"){
		todos.forEach(function(feature){
			name_list.push(feature.get("nombre"));
		});
	}else{
		todos.forEach(function(feature){
			if (feature.get("id") == uga){
				name_list.push(feature.get("nombre"));
			}
		});
	}
	
	
	texto = "<h1>Selección </br> ("+name_list.length+")</h1>";
	name_list.forEach(function(name){
		texto = texto + '<div class="item">'+ name +'</div>'
	});
	lista = document.getElementById("selected");
	lista.innerHTML = texto;
	if (uga == "All"){
		console.log("todos")
	}else{
		pcz.highlight(pcz.data().filter(function(d) {
			return d.id === uga;
			}));

		vectorSource.clear();
		estosFeatures = todos.filter(function (feature) {return feature.get('id') == uga;});
		vectorSource.addFeatures(estosFeatures);
	}
	
	updateCharts(name_list);
	
 });

