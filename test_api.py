import requests
import json
import os

from datetime import datetime

# url = 'https://diagrama.yucatan.gob.mx/apiv2/datospoety'
url = 'https://diagrama.yucatan.gob.mx:8181/apiv2/datospoety'
header = {'token': 'ceee76f0dd57e3a8225d34ea07e935d6'}

path_uga = os.path.join(os.getcwd(), 'POETY.geojson')
path_estatal = os.path.join(os.getcwd(), 'POETY_ESTATAL.geojson')

# path_uga = os.path.join(os.getcwd(), 'data', 'ejemplo1.geojson')
# path_estatal = os.path.join(os.getcwd(), 'data', 'ejemplo1.geojson')

print(path_uga)
# with open("/home/fidel/GitLab/monitoreo_yucatan/data/ejemplo1.geojson","r") as fp:#r - open file in read mode
with open(path_uga,"r") as fp:#r - open file in read mode
 el_geojson_ugas = json.load(fp)

with open(path_estatal,"r") as fp:#r - open file in read mode
 el_geojson_estatal = json.load(fp)
#el_geojson = {"type": "FeatureCollection","name": "ejemplo1","crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },"features": [{ "type": "Feature", "properties": { "id_uga": 94 }, "geometry": { "type": "MultiPolygon", "coordinates": [ [ [ [ -90.02496875688314, 20.606470729445217 ], [ -89.968388879083591, 20.605646897874504 ], [ -89.967063497775356, 20.586713181710433 ], [ -89.958473703471384, 20.588665780838429 ], [ -89.955424828201942, 20.579690953485795 ], [ -89.964972428094001, 20.577722135514005 ], [ -89.983610619774566, 20.550321137779587 ], [ -89.996153157762407, 20.554617910445412 ], [ -90.004669751710182, 20.549054271012551 ], [ -90.004153165234897, 20.571629970129127 ], [ -90.020566137724941, 20.57766275630366 ], [ -90.033629710159317, 20.608124139137256 ], [ -90.02496875688314, 20.606470729445217 ] ] ] ] } },{ "type": "Feature", "properties": { "id_uga": 180 }, "geometry": { "type": "MultiPolygon", "coordinates": [ [ [ [ -89.190479403366083, 20.591975513503247 ], [ -89.19035043670489, 20.582946067472506 ], [ -89.249811175388359, 20.583086009525452 ], [ -89.250089438106798, 20.602047067211284 ], [ -89.229716185764076, 20.651980834799279 ], [ -89.203208306422326, 20.676705002425699 ], [ -89.180181752976992, 20.676997844890899 ], [ -89.180484044424375, 20.630935587432862 ], [ -89.189933472988642, 20.620881835445093 ], [ -89.190479403366083, 20.591975513503247 ] ] ] ] } }]}

# lstSectores = ["agri","ener","hidr","indu","manejo","mine","porc","turi","urba","vial"]
dictSectores = {
        "agri": [ "AGRÍCOLA", "AGROINDUSTRIA" ],
        "ener": [
'CONSTRUCCIÓN DE ESTACIONES O SUBESTACIONES ELÉCTRICAS',
'LÍNEA DE DISTRIBUCIÓN ELÉCTRICA',
'LÍNEA DE TRANSMISIÓN ELÉCTRICA',
'OBRAS DE TRANSMISIÓN O SUBTRANSMISIÓN ELÉCTRICA'
        ],
        "hidr": [
'LAVADEROS AUTOMATIZADOS',
'PATIO DE MANIOBRAS LOGÍSTICAS',
'PLANTAS DE TRATAMIENTO',
'PLANTAS POTABILIZADORAS'
        ],
        "indu": [
'AMPLIACIÓN DE INSTALACIONES INDUSTRIALES',
'FÁBRICAS DE ESTRUCTURAS METÁLICAS Y MUEBLES METÁLICOS',
'NAVES INDUSTRIALES',
'PARQUES INDUSTRIALES',
'PLANTAS INDUSTRIALES'
        ],
        "resi": [
'BANCO DE DEPÓSITO Y/O TIRO',
'CENTROS DE ACOPIO DE RESIDUOS ORGÁNCIOS E INORGÁNICOS',
'ESTACIONES DE TRANSFERENCIA',
'SITIOS DE DISPOSICIÓN DE RESIDUOS SÓLIDOS URBANOS Y DE MANEJO ESPECIAL'
        ],
        "mine": [
'AMPLIACIÓN BANCO DE MATERIALES',
'BANCO DE MATERIAL PÉTREO (APROVECHAMIENTO Y EXPLOTACIÓN)'
        ],
        "porc": [
'CENTRO DE PRODUCCIÓN PECUARIA',
'CENTRO DE PRODUCCIÓN AVÍCOLA',
'CENTRO DE PRODUCCIÓN PORCÍCOLA'
        ],
        "turi": [
'DESARROLLOS ECOTURÍSTICOS',
'INFRAESTRUCTURA TURÍSTICA Y HABITACIONAL'
        ],
        "urba": [
'BODEGAS DE ARTÍCULOS Y SUSTANCIAS NO RIESGOSAS',
'CENTRALES DE ABASTO',
'CLÍNICAS Y HOSPITALES',
'CONSTRUCCIÓN DE CLUBES DEPORTIVOS Y ESTADIOS',
'CREMATORIOS',
'DEPARTAMENTOS HABITACIONALES',
'ESCUELAS, INSTITUCIONES EDUCATIVAS Y DE INVESTIGACIÓN',
'FRACCIONAMIENTOS',
'HABITACIONAL',
'HOTEL',
'LOCALES COMERCIALES',
'LOTIFICACIÓN',
'MOTELES',
'PLAZAS COMERCIALES',
'RASTROS',
'SUPERMERCADOS Y MERCADOS ORGÁNICOS'
        ],
        "vial": [
'AMPLIACIÓN DE VÍAS DE COMUNICACIÓN ESTATALES',
'AMPLIACIÓN DE CAMINO',
'CAMINO DE ACCESO',
'CAMINOS MUNICIPALES Y RURALES',
'CONSTRUCCIÓN DE VÍAS DE COMUNICACIÓN ESTATALES',
'RECONSTRUCCIÓN DE VIAS DE COMUNICIACIÓN ESTATALES'
        ],
}

# UGA
for sector, lstSubSectores in dictSectores.items():
        # print(key, value)
        # print(datetime.datetime.now())
        t2 = datetime.now().strftime("%d%m%Y")
        data = {
                "geom": el_geojson_ugas,
                "t1": "",
                "t2": "",
                "sector": lstSubSectores
                }

        x = requests.post(url, json = data, headers=header)
        response = x.text
        print(response)

        pathDir = 'jsonfiles/{}'.format(datetime.now().year)
        if not os.path.exists(pathDir):
                os.makedirs(pathDir)

        with open('jsonfiles/{}/{}.json'.format(datetime.now().year, sector), 'w') as f:
                f.write(response)

# ESTATAL
for sector, lstSubSectores in dictSectores.items():
        # print(key, value)
        # print(datetime.datetime.now())
        t2 = datetime.now().strftime("%d%m%Y")
        data = {
                "geom": el_geojson_estatal,
                "t1": "",
                "t2": "",
                "sector": lstSubSectores
                }

        x = requests.post(url, json = data, headers=header)
        response = x.text
        print(response)

        pathDir = 'jsonfiles/{}'.format(datetime.now().year)
        if not os.path.exists(pathDir):
                os.makedirs(pathDir)

        with open('jsonfiles/{}/{}_estatal.json'.format(datetime.now().year, sector), 'w') as f:
                f.write(response)

# PARALLELS COORDINATES y MAPA
data = {
        "geom": el_geojson_ugas,
        "t1": "",
        "t2": "",
        "sector": ""
        }
x = requests.post(url, json = data, headers=header)
response = x.text

pathDir = 'jsonfiles/{}'.format(datetime.now().year)

if not os.path.exists(pathDir):
        os.makedirs(pathDir)

with open('jsonfiles/{}/parallel.json'.format(datetime.now().year), 'w') as f:
        f.write(response)
