import geopandas as gpd
import pandas as pd
import os

carpeta = "/Users/fidel/monitoreo_yucatan/dummy_response"

os.chdir(carpeta)


#################################################################################################
##  crear los csvs de la sintesis por sector
#################################################################################################


## funcion que lee el geojson del sector, extrae el dataframe y lo transforma de wide a long
## y agrega la columna sector con el valor del correspondiente sector
def wide2long(sector):
    gdf = gpd.read_file(sector+'.geojson')
    df = pd.DataFrame(gdf.drop(columns='geometry'))
    campos = list(df.columns)
    campos.remove("id_uga")
    df_long = pd.melt(df, id_vars='id_uga', value_vars=campos)
    df_long['sector'] = sector
    return(df_long)

## hacer un dataframe long con todos los datos
sectores = ["agri","ener","hidr","indu","manejo","mine","porc","turi","urba","vial"]
longs_list = []
for sector in sectores:
    longs_list.append(wide2long(sector))
long = pd.concat(longs_list)
#long.to_csv('long.csv')  

## obtener los campos que no son id_uga, aquí alternativamente se puede dar la lista de variables que 
## van en la sistesis por sector
gdf = gpd.read_file('agri.geojson')
df = pd.DataFrame(gdf.drop(columns='geometry'))
nombres_csvs = list(df.columns)
nombres_csvs.remove("id_uga")

## para cada campo crear un csv
for variable in nombres_csvs:
    df_long = long[long['variable'] == variable] 
    df = pd.pivot(df_long, index='id_uga', columns='sector', values='value')
    df = df.reset_index()
    df.to_csv(variable+'.csv', index = False)  


#################################################################################################
##  crear el geojson y csv para el mapa y el parallel coordinates
#################################################################################################


