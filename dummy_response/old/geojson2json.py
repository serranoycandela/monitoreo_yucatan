import geopandas as gpd
import pandas as pd
import os
import json

carpeta = "/home/fidel/GitLab/monitoreo_yucatan/dummy_response"

os.chdir(carpeta)


#################################################################################################
##  crear los csvs de la sintesis por sector
#################################################################################################


## funcion que lee el geojson del sector, extrae el dataframe y lo transforma de wide a long
## y agrega la columna sector con el valor del correspondiente sector
def geo2json(sector):
    gdf = gpd.read_file(sector+'.geojson')
    df = pd.DataFrame(gdf.drop(columns='geometry'))
    respuestas = []
    campos = list(df.columns)
    campos.remove("id_uga")
    for index, row in df.iterrows():
        res = {}
        for campo in campos:
            res[campo] = row[campo]
        respuestas.append({"id_uga": row["id_uga"], "resultado": res})
    return respuestas

sectores = ["agri","ener","hidr","indu","manejo","mine","porc","turi","urba","vial"]
for sector in sectores:
    sector_json = geo2json(sector)
    json_object = json.dumps(sector_json, indent = 4)
  

    with open(sector+".json", "w") as outfile:
        outfile.write(json_object)