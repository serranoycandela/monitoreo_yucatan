from fiona import listlayers
import geopandas as gpd
import pandas as pd
import os
import json

carpeta = os.path.join(os.getcwd(), "jsonfiles","2022")

os.chdir(carpeta)


#################################################################################################
##  crear los csvs de la sintesis por sector
#################################################################################################


## funcion que lee el json del sector, extrae el dataframe y lo transforma de wide a long
## y agrega la columna sector con el valor del correspondiente sector
def wide2long(sector):
    with open(sector+".json") as f:
        data = json.load(f)
    
    lista = []
    for uga in data:
        dicc = {"id_uga": uga["id_uga"]}
        for variable in uga["resultado"]:  
            dicc[variable] = uga["resultado"][variable]
        lista.append(dicc)
    
    df = pd.DataFrame(lista)
    campos = list(df.columns)
    campos.remove("id_uga")
    df_long = pd.melt(df, id_vars='id_uga', value_vars=campos)
    df_long['sector'] = sector
    return(df_long)



## hacer un dataframe long con todos los datos
sectores = ["agri","ener","hidr","indu","resi","mine","porc","turi","urba","vial"]
longs_list = []
for sector in sectores:
    longs_list.append(wide2long(sector))
long = pd.concat(longs_list)
#long.to_csv('long.csv')  

## obtener los campos que no son id_uga, aquí alternativamente se puede dar la lista de variables que 
## van en la sistesis por sector
with open("agri.json") as f:
    data = json.load(f)
nombres_csvs = data[0]["resultado"].keys()

## para cada campo crear un csv
for variable in nombres_csvs:
    df_long = long[long['variable'] == variable] 
    df = pd.pivot(df_long, index='id_uga', columns='sector', values='value')
    df = df.reset_index()
    df.to_csv(variable+'.csv', index = False)  

#################################################################################################
##  crear los csvs de la sintesis por sector estatal
#################################################################################################

sectores = ["agri","ener","hidr","indu","resi","mine","porc","turi","urba","vial"]
longs_list = []
for sector in sectores:
    longs_list.append(wide2long(sector+"_estatal"))
long = pd.concat(longs_list)
#long.to_csv('long.csv')  

## obtener los campos que no son id_uga, aquí alternativamente se puede dar la lista de variables que 
## van en la sistesis por sector
with open("agri.json") as f:
    data = json.load(f)
nombres_csvs = data[0]["resultado"].keys()

## para cada campo crear un csv
for variable in nombres_csvs:
    df_long = long[long['variable'] == variable] 
    df = pd.pivot(df_long, index='id_uga', columns='sector', values='value')
    df = df.reset_index()
    df.to_csv(variable+'_estatal.csv', index = False)


#################################################################################################
##  crear el geojson y csv para el mapa y el parallel coordinates
#################################################################################################



with open("parallel.json") as f:
    data = json.load(f)
    
lista = []
for uga in data:
    dicc = {"id_uga": uga["id_uga"]}
    for variable in uga["resultado"]:  
        dicc[variable] = uga["resultado"][variable]
    lista.append(dicc)

df = pd.DataFrame(lista)
df = df.drop(columns=['sup_desm', 'sup_infr', 'agua_consum', 'agua_res', 'res_urb'])
df = df.rename(columns={"sup_tot_camb_uso": "t_camb_uso", "cuerp_agua_uso": "c_agua_uso"})
				

df.to_csv("parallel.csv", index=False)

pdf = pd.read_csv("parallel.csv") # CSV file

carpeta = "/home/fidel/GitLab/monitoreo_yucatan"

os.chdir(carpeta)

gdf = gpd.read_file("pal_join.geojson") # geojson file

layer_gdf = gdf.merge(pdf, on="id_uga")
layer_gdf.to_file("layer.json", driver="GeoJSON")
data_df = pd.DataFrame(layer_gdf.drop(columns='geometry'))
data_df.to_csv("data.csv", index=False)