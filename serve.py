#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from datetime import datetime
from flask import Flask, render_template, jsonify, redirect, url_for, request, send_from_directory
from werkzeug.utils import secure_filename
import shapefile
from jinja2 import Environment, FileSystemLoader
import json
from simpledbf import Dbf5
import hashlib
from json import dumps

app = Flask(__name__)
app.config.from_object(__name__)

base_path = os.path.dirname(os.path.realpath(__file__))

app.config['UPLOAD_FOLDER'] = os.path.join(base_path, 'uploads')


template_path = os.path.join(base_path, 'templates')
env = Environment(loader=FileSystemLoader(template_path))


url = None

@app.route("/")
def root():
    if url is not None:
        return redirect(url + '/monitoreo/2022/All', code=302)
    else:
        return redirect('/monitoreo/2022/All', code=302)


@app.route('/monitoreo/<map_id>')
def table(map_id):
    
    layer_url = "/uploads/%s/layer.json" % map_id
    data_url = "/uploads/%s/data.csv" % map_id
    year = map_id
    template = env.get_template('base.html')
    return template.render(layer_url=layer_url, data_url=data_url, year=year)

@app.route('/monitoreo/<map_id>/<uga_id>')
def table2(map_id,uga_id):
    
    layer_url = "/uploads/%s/layer.json" % map_id
    data_url = "/uploads/%s/data.csv" % map_id
    year = map_id
    uga = uga_id
    template = env.get_template('base.html')
    return template.render(layer_url=layer_url, data_url=data_url, year=year, uga = uga)


@app.route('/uploads/<path:path>')
def serve_uploads(path):
    return send_from_directory('uploads', path)

@app.route('/static/<path:path>')
def serve_static(path):
    return send_from_directory('static', path)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=5004,
        debug=True
    )
